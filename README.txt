slides presentation
--------------------

Maintainers:
 Marouan Hammami mh.marouan@gmail.com / https://www.drupal.org/user/3203501

SUMMARY:

This module is for easily creating beautiful presentations using HTML.

Usage
Create your presentation by type "presentation".
Create list of slide by type "slide" per presentation.
View your presentation page.

Installation

Install Libraries API 2.x
Download reveal.js
Put the folder in a libraries directory
Ensure you have file /libraries/reveal/js/reveal.js
Install slides presentation module